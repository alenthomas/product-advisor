from django.apps import AppConfig


class ReleventkeywordsConfig(AppConfig):
    name = 'releventkeywords'

from keywordsfinder import crawl
from searchweb import youtube
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def go_fetch(product_url):
  if not product_url:
    return []
  return crawl.findKeyWords(product_url)

def youtube_results(strings):
  try:
    y_results = youtube.youtube_search(strings)
    if len(y_results[1]) > 1:
      return y_results[1]
  except Exception as e:
    logger.debug(e)
    return []

def youtube_comments(video_id):
  if not video_id:
    return []
  try:
    comments = youtube.comment_threads_list_by_video_id(video_id)
    if len(comments[1]) > 1:
      return comments[1]
  except Exception as e:
    logger.debug(e)
    return []

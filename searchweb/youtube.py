import os
from apiclient.discovery import build
from apiclient.errors import HttpError
from oauth2client.tools import argparser
# from django.conf import YOUTUBE_KEY
from django.conf import settings


DEVELOPER_KEY = "AIzaSyA5P9Mq92dGEQXWRAh409kBlBrhoRGDZBE"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"




def youtube_search(q, max_results=50, order="relevance", token=None, location=None, location_radius=None):

  youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)

  search_response = youtube.search().list(
    q=q,
    type="video",
    pageToken=token,
    order = order,
    part="id,snippet",
    maxResults=max_results,
    location=location,
    locationRadius=location_radius

  ).execute()



  videos = []

  for search_result in search_response.get("items", []):
    if search_result["id"]["kind"] == "youtube#video":
      videos.append(search_result)
  try:
      nexttok = search_response["nextPageToken"]
      return(nexttok, videos)
  except Exception as e:
      nexttok = "last_page"
      return(nexttok, videos)


def geo_query(video_id):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey=DEVELOPER_KEY)

    video_response = youtube.videos().list(
        id=video_id,
        part='snippet, recordingDetails, statistics'

    ).execute()

    return video_response

def comment_threads_list_by_video_id(video_id):
  print('in ', 'comment_threads_list_by_video_id')
  client = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
    developerKey=DEVELOPER_KEY)
  # See full sample for function
  # kwargs = remove_empty_kwargs(**kwargs)

  comment_response = client.commentThreads().list(
    part='snippet,replies',
    videoId=video_id
  ).execute()

  comments = [];
  print('------------------', comment_response)
  for comment_result in comment_response.get("items", []):
    if comment_result["kind"] == "youtube#commentThread":
      comments.append(comment_result)
  try:
      nexttok = comment_response["nextPageToken"]
      return(nexttok, comments)
  except Exception as e:
      nexttok = "last_page"
      return(nexttok, comments)

from django.shortcuts import render
from django.http import JsonResponse
import logging
from .youtube import youtube_search


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


# Create your views here.

def search_web(request):
  return JsonResponse({'name': 'alen'})

def search_youtube(strings):
  try:
    results = youtube_search(strings)
    if results:
      return results[1]
  except Exception as e:
    logger.debug(e)
    return None


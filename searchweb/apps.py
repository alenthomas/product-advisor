from django.apps import AppConfig


class SearchwebConfig(AppConfig):
    name = 'searchweb'

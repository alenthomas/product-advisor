import React, { Component } from 'react';
var Spinner = require('react-spinkit');

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {value: '', videos: '', comments: '', videoFetched: false, loading: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    this.setState({loading: true})
    fetch('/moreinfo', {
      method: 'POST',
      headers : {
                'Content-Type': 'application/json',
            },
      body: JSON.stringify({ productname: this.state.value }),

    }).then(e => {
      return e.json();
    }).then(e => {
      console.log(e)
      this.setState({videos: e, videoFetched: true, loading: false})
    }).catch(e => {
      console.log(e)
    })
  }

  renderVideos({data: videos}) {
    const videolist = videos.map(e => (
    <div className="column is-one-quarter">
      <div className="card">
        <div className="card-content">
          <h5 className="title is-6">{e.snippet.title} </h5>
          <figure className="image is-128x128">
          <a href={`https://www.youtube.com/watch?v=${e.id.videoId}`} target="_blank">
            <img src={`${e.snippet.thumbnails.default.url}`} />
            </a>
          </figure>
        </div>
      </div>
    </div>
    )
    )
    return videolist;
  }

  render() {
    if(this.state.loading) {
      return (
        <div className="level">
          <div className="level-item has-text-centered">
              <Spinner name='double-bounce' />
              <h6> loading </h6>
          </div>
        </div>
      )
    }
    return (
      <div>
        <section className="hero is-primary">
          <div className="hero-body">
            <div className="container">
              <h1 className="title is-1">Product Advisor</h1>
              <h2 className="subtitle is-4">Find out what youtube thinks about your product</h2>
            </div>
          </div>
        </section>
        <div className="container">
          <form onSubmit={this.handleSubmit} className="control">
            <div className="field">
              <label className="label">Enter product URL</label>
              <div className="control">
                <input className="input" name="productname" type="text" value={this.state.value} onChange={this.handleChange} />
              </div>
            </div>
            <div className="field">
              <div className="control">
                <input className="button" type="submit" value="Submit" />
              </div>
            </div>
           </form>
           <br />
           <div className="columns is-multiline is-mobile">
             {this.state.videoFetched? this.renderVideos(this.state.videos): false}
           </div>
        </div>
      </div>
    );
  }
}

export default App;

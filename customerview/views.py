from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt

from django.http import JsonResponse
from wiring import controller
from keywordsfinder import crawl
import logging
import json

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Create your views here.

# def home(request):
#   return JsonResponse({'name': 'alen'})
@csrf_exempt
def home(request):
  return render(request, 'customerview/index.html', {'name': 'alen'})

@csrf_exempt
def more_info(request):
  # print(request.POST, '\n', request.body, '\n', type(request.body))
  form_data = json.loads(request.body)
  print(form_data)
  print(form_data['productname'])
  print(request.is_ajax())
  if request.body:
    data = [];
    try:
      keywords = controller.go_fetch(form_data['productname'])
      print('try', keywords)
      data = controller.youtube_results(keywords)
      print(data)
    except Exception as e:
      print('exception', data)
      logger.debug(e)
    finally:
      print('finally', data)
      return JsonResponse({'data': data})
  return JsonResponse({'data': []})

@csrf_exempt
def more_comments(request):
  print(request.GET.get('video_id'))
  if request.GET:
    data = [];
    try:
      data = controller.youtube_comments(request.GET.get('video_id', None))
    except Exception as e:
      logger.debug(e)
    finally:
      return JsonResponse({'data': data})
  return JsonResponse({'data': []})
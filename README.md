# Product Advisor

Product Advisor takes a product page url as input and returns youtube related searches for it.

# Installation
Instructions to run the application.
  - clone the repository

## Django
  - Setup a virtual env for Python
  - Execute the following commands

```shell
$ pip install -r requirements.txt
```

## ReactJs
   - change current directory to UI

```shell
$ cd ui
```
  - use yarn/npm to install the dependencies

```shell
$ yarn
```
# Running
  - to run the django-dev-server from the current directory run

```shell
$ python manage.py runserver
```
  - this should start the Django dev server on your local `http://127.0.0.1:8000`
  - to run the react-dev-server change the directory to `ui`

```shell
$ cd ui
```
- run the react dev-server

```shell
$ yarn start
```
  - this should start the react-dev-server on your browser `http://127.0.0.1:3000`
  - enter a product page to the input field and submit
  - eg: https://www.bose.com/en_us/products/headphones/earphones/soundsport-free-wireless.html#v=soundsport_free_wireless_black
  - this will give you the first 50 relevent youtube videos `Bose Soundsport wireless`

# Tech stack
  - Python 3.7
  - Django 2.0.7
  - ReactJs 16.4.1
  - Beautiful-Soup 4.6.0
  - Google-Api-Python-Client 1.7.4


# Todos
 - Implement the user comments sections for a particular card
 - Implement better relevent keywords filter
 - Add filter for positive feedbacks
 - Bundle reactjs and add it to django static files

# License
----

MIT


"""productAdvisor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from customerview import views as cust_view
from filterpositivecontent import views as filter_positive
from keywordsfinder import views as kw
from releventkeywords import views as relevent_kw
from searchweb import views as web

urlpatterns = [
    path('admin/', admin.site.urls),
    # keywordsfinder
    path('kw_search/', kw.kw_search),
    # releventkeywords
    path('kw_filter/', relevent_kw.kw_filter ),
    # searchweb
    path('search_web/', web.search_web ),
    # filterpositivecontent
    path('pick_relevent/', filter_positive.pick_relevent),
    # customerview
    path('', cust_view.home),
    path('moreinfo', cust_view.more_info),
    path('morecomments', cust_view.more_comments),

]

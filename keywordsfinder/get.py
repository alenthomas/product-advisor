import requests
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def geturl(url):
  try:
    r = requests.get(url)
    return r.text
  except Exception as e:
    logger.debug(e)
    return None
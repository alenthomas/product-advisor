from django.apps import AppConfig


class KeywordsfinderConfig(AppConfig):
    name = 'keywordsfinder'

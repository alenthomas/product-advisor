from .get import geturl
from urllib import parse
import logging
from bs4 import BeautifulSoup

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def adddomain(string):
  try:
    p = parse.urlparse(string)
    domain_list = p.netloc.split('.')[1]
    return domain_list
  except Exception as e:
    logger.debug(e)
    return ''

def extract_keys(text):
  keys = [];
  soup = BeautifulSoup(text, 'html.parser')
  try:
    keys.append(soup.title.string)
    seo_string = soup.find(attrs={"name": "keywords"})['content']
    seo = seo_string.split(',')
    keys += seo
    return keys
  except Exception as e:
    logger.debug(e)
    return keys

def findKeyWords(url):
  keywords = [];
  keywords.append(adddomain(url))
  text = geturl(url)
  if text:
    important_keys = extract_keys(text)
    keywords += important_keys
    return keywords
  else:
    return keywords
